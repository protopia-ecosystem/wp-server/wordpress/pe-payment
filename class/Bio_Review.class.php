<?php 

class Bio_Review extends SMC_Post
{
	static function get_type()
	{
		return BIO_REVIEW_TYPE;
	}
	static function init()
	{
		
		add_action('init', 						[ __CLASS__, 'register_all' ], 3);	
		add_action('registered_taxonomy', 		[ __CLASS__, 'registered_taxonomy' ], 3, 3);	
		parent::init();
	}
	function registered_taxonomy( $taxonomy, $object_type, $args )
	{
		if($taxonomy == BIO_COURSE_TYPE)
			register_taxonomy_for_object_type( BIO_COURSE_TYPE, BIO_REVIEW_TYPE);
	}
	static function register_all()
	{
		$labels = array(
			'name'               => __("Review", BIO), // Основное название типа записи
			'singular_name'      => __("Review", BIO), // отдельное название записи типа Book
			'add_new'            => __("add Review", BIO), 
			'all_items' 		 => __('Reviews', BIO),
			'add_new_item'       => __("add Review", BIO), 
			'edit_item'          => __("edit Review", BIO), 
			'new_item'           => __("add Review", BIO), 
			'view_item'          => __("see Review", BIO), 
			'search_items'       => __("search Review", BIO), 
			'not_found'          => __("no Reviews", BIO), 
			'not_found_in_trash' => __("no Reviews in trash", BIO), 
			'parent_item_colon'  => '',
			'menu_name'          => __("Reviews", BIO), 
		);
		register_post_type(
			static::get_type(), 
			[
				'labels'             => $labels,
				'taxonomies'		 => [ BIO_COURSE_TYPE ],
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => 'kpdbio_page',
				'query_var'          => true,
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				'menu_position'      => 14.211,
				"menu_icon"			 => "dashicons-megaphone",
				'supports'           => array( 'title', 'editor', 'author' ),
				"rewrite"			 => ["slug" => ""]
			]
		);
		
	}
}