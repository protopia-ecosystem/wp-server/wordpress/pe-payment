<?php 

class PE_ContactFormMessage extends SMC_Post
{
	static function get_type()
	{
		return PE_CONTACT_FORM_MESSAGE;
	}
	static function init()
	{
		add_action('init', 						[ __CLASS__, 'register_all' ], 3);	
		add_filter("smc_post_view_admin_edit", 	[ __CLASS__, "smc_post_view_admin_edit"], 10, 5 );
		add_filter("smc_post_fill_views_column", [ __CLASS__, "smc_post_fill_views_column"], 10, 5 );
		parent::init();
	}
	static function register_all()
	{
		$labels = [
			'name'               => __("Contact Form message Form message", BIO), 
			'singular_name'      => __("Contact Form message", BIO), 
			'add_new'            => __("add Contact Form message", BIO), 
			'all_items' 		 => __('Contact Form messages', BIO),
			'add_new_item'       => __("add Contact Form message", BIO), 
			'edit_item'          => __("edit Contact Form message", BIO), 
			'new_item'           => __("add Contact Form message", BIO), 
			'view_item'          => __("see Contact Form message", BIO), 
			'search_items'       => __("search Contact Form message", BIO), 
			'not_found'          => __("no Contact Form messages", BIO), 
			'not_found_in_trash' => __("no Contact Form messages in trash", BIO), 
			'parent_item_colon'  => '',
			'menu_name'          => __("Contact Form messages", BIO), 
		];
		register_post_type(
			static::get_type(), 
			[
				'labels'             => $labels,
				'taxonomies'		 => [ ],
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => 'pe_payment_page',
				'query_var'          => true,
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				'menu_position'      => 19,
				"menu_icon"			 => "dashicons-megaphone",
				'supports'           => [ 'title', 'editor', 'author' ],
				"rewrite"			 => [ "slug" => "" ]
			]
		);
	}
	static function get_post( $p )
	{
		$ins 	= static::get_instance($p);
		$matrix = parent::get_post($p);
		$matrix['fields'] = $ins->get_meta("fields");
		$matrix['matrix'] = $ins->get_meta("matrix");
		return $matrix;
	}
	static function smc_post_fill_views_column( $meta, $type, $obj, $column_name, $post_id  )
	{
		if($type ==  "cf_message_field" )
		{
			$meta = preg_replace( "/'/i", '"',  $meta );
			$fields = json_decode( $meta );
			$text = "<div class=''>";				
			for ( $i = 0; $i < count($fields); $i++ )
			{		
				switch($fields[$i]->type)
				{
					case "file_loader":
						$content = "<img src='" . $fields[$i]->value . "' style='width:100%; height:auto;' >";
						break;
					case "string":
					case "time":
					case "phone":
					default:
						$content = $fields[$i]->value;
				}		
				$text .= "<div class='row'>
					<div class='col-6 p-1'>" .
						$fields[$i]->label .
					"</div>
					<div class='col-6 p-1'>" .
						$content .
					"</div>
				</div>";				
			}
			$text .= "</div>";
			return $text;
		}
		return $meta;
	}
	static function smc_post_view_admin_edit( $text, $obj, $key, $value, $meta )
	{
		if( $value['type'] == "cf_message_field" )
		{
			$meta = preg_replace( "/'/i", '"',  $meta );
			$fields = json_decode( $meta );
			$text = "<div class='card'>";				
			for ( $i = 0; $i < count($fields); $i++ )
			{		
				switch($fields[$i]->type)
				{
					case "file_loader":
						$content = "<img src='" . $fields[$i]->value . "' style='width:100%; height:auto;' >";
						break;
					case "string":
					case "time":
					case "phone":
					default:
						$content = $fields[$i]->value;
				}		
				$text .= "<div class='row'>
					<div class='col-4 p-1'>" .
						$fields[$i]->label .
					"</div>
					<div class='col-3 p-1'>" .
						$fields[$i]->type .
					"</div>
					<div class='col-5 p-1'>" .
						$content .
					"</div>
				</div>";				
			}
			$text .= "</div>";
			
		}
		if( $value['type'] == "cf_message_matrix" )
		{
			$text = $meta;
		}
		return $text;
	}
}

class CFMessageField
{
	private $fields = [];
	private $matrix = [];
	
	function __construct( $data = [] )
	{
		$fields	= $data->fields; 
		$matrix	= $data->matrix; 
	}
}