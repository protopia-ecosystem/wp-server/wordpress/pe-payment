<?php 

class Bio_Testinomial extends SMC_Post
{
	static function get_type()
	{
		return BIO_TESTINOMIAL_TYPE;
	}
	static function init()
	{
		add_action('init', 						[ __CLASS__, 'register_all' ], 2);	
		parent::init();
	}
	static function register_all()
	{
		$labels = array(
			'name'               => __("Testinomial", BIO), // Основное название типа записи
			'singular_name'      => __("Testinomial", BIO), // отдельное название записи типа Book
			'add_new'            => __("add Testinomial", BIO), 
			'all_items' 		 => __('Testinomials', BIO),
			'add_new_item'       => __("add Testinomial", BIO), 
			'edit_item'          => __("edit Testinomial", BIO), 
			'new_item'           => __("add Testinomial", BIO), 
			'view_item'          => __("see Testinomial", BIO), 
			'search_items'       => __("search Testinomial", BIO), 
			'not_found'          => __("no Testinomials", BIO), 
			'not_found_in_trash' => __("no Testinomials in trash", BIO), 
			'parent_item_colon'  => '',
			'menu_name'          => __("Testinomials", BIO), 
		);
		register_post_type(
			static::get_type(), 
			[
				'labels'             => $labels,
				'taxonomies'		 => [ BIO_COURSE_TYPE ],
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => 'kpdbio_page',
				'query_var'          => true,
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				'menu_position'      => 4,
				"menu_icon"			 => "dashicons-megaphone",
				'supports'           => array( 'title','editor','author','thumbnail','excerpt','comments' ),
				"rewrite"			 => ["slug" => ""]
			]
		);
	}
	static function get_post( $p )
	{
		$ins 	= static::get_instance($p);
		$matrix = parent::get_post($p);
		$matrix['raiting'] = $ins->get_meta("raiting");
		$matrix['display_name'] = $ins->get_meta("display_name");
		return $matrix;
	}
}