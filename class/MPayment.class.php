<?php
//TODO :: проверка наличия плагинов wp-qraphql, wp-graphql-cors, wp-graphql-jwt-authentication

require_once(__DIR__ . "/../../pe-graphql/vendor/autoload.php");

use GraphQL\Error\ClientAware;
use GraphQL\Utils\BuildSchema;
use GraphQL\Utils\SchemaPrinter;
use GraphQL\Type\Definition\ObjectType;
use GraphQL\Type\Definition\Type;

class MPayment extends SMC_Post
{
	static function get_type()
	{
		return M_PAYMENT_TYPE;
	}
	static function init()
	{
		$typee = static::get_type();
		add_action('init', 						[ __CLASS__, 'register_all' ], 20);	
		
		if(Bio::$options['pay_sistem'] == "1")
		{
			add_action("pe_graphql_make_schema", 	[ __CLASS__, "exec_graphql_robocassa"], 8);
			add_action('parse_request', 			[ __CLASS__, 'exec_result_robocassa']);
		}
		else
		{
			add_action("pe_graphql_make_schema", 	[ __CLASS__, "exec_graphql_yandex_money"], 8);
			add_action('parse_request', 			[ __CLASS__, 'exec_result_yandex_money']);
		}
		//
		add_action("bio_payment_bad_connection",	[ __CLASS__, "bio_payment_bad_connection"], 10, 4 );
		add_action("bio_payment_bad_goods",			[ __CLASS__, "bio_payment_bad_goods"], 10, 2 );
		add_action("bio_payment_bad_goods_price",	[ __CLASS__,"bio_payment_bad_goods_price"], 10, 3 );
		add_action("bio_payment_success",			[ __CLASS__, "bio_payment_success"], 10, 4);
		parent::init();
	}
	static function bio_payment_success($term_id, $user_id, $out_summ )
	{
		$className	= Bio::$options['goods_type'] ? Bio::$options['goods_type'] : "Bio_Course";
		$term 		= $className::get_instance($term_id);
		$cname 		= $className::get_title_name();
		
		static::insert([
			"post_title"	=> $term->body->$cname,
			"post_content"	=> __("Успешный платёж", MCOURSES),
			"post_author"	=> $user_id,
			"summae"		=> $out_summ,
			"element_id"	=> $term_id,
			"element_type"	=> Bio::$options['goods_type'] ? Bio::$options['goods_type'] : "Bio_Course",
			"is_success"	=> 1
		]);
		$prev_course_id 	= get_user_meta($user_id, "current_course", true );
		static::db_insert($user_id, $className, $term_id, $out_summ, $term->body->$cname, 1);
		
		// for pe_payment
		//pe_payment::set_user_current_course( $user_id, $term_id, $prev_course_id);
		
		// for Eugeny
		MSchool::set_user_facultet($user_id, $term_id);
		
		
	}
	static function bio_payment_bad_goods_price($term_id, $user_id, $out_summ )
	{
		$term = Bio_Course::get_instance($term_id);
		static::insert([
			"post_title"	=> $term->body->name,
			"post_content"	=> __("Payment bad: does not match unsummae.", MCOURSES),
			"post_author"	=> $user_id,
			"summae"		=> $out_summ,
			"element_id"	=> $term_id,
			"element_type"	=> Bio::$options['goods_type'] ? Bio::$options['goods_type'] : "Bio_Course",
			"is_success"	=> 0
		]);
	}
	
	
	static function exec_graphql_robocassa()
	{
		PEGraphql::add_query( 
			'getRobokassaRedirectUrl', 
			[
				'description' => __( '', BIO ),
				'type' 		=> Type::string(),
				'args'     	=> [ 
					"course_id" => [ "type" => Type::int() ],
				],
				'resolve' 	=> function( $root, $args, $context, $info ) use($class_name, $post_type, $val)
				{		
					$mrh_login = Bio::$options['robocassa_key'];
					$mrh_pass1 = Bio::$options['robocassa_password_1'];

					// номер заказа
					// number of order
					$inv_id = 0;

					// описание заказа
					// order description
					$term_id 	= $args['course_id'];
					$className 	= Bio::$options['goods_type'] ? Bio::$options['goods_type'] : "Bio_Course";
					$course 	= $className::get_instance($args['course_id']);
					$cname 		= $className::get_title_name();
					$inv_desc 	= $course->body->$cname;//"ROBOKASSA Advanced User Guide";
					
					// сумма заказа
					// sum of order
					$out_summ = $course->get_meta("price");//"8.96";
					$user_id = get_current_user_id();
					
					//wp_die( $out_summ );
					
					if( !$out_summ )
					{
						//$prev_course_id 	= get_user_meta($user_id, "current_course", true );
						//pe_payment::set_user_current_course( $user_id, $term_id, $prev_course_id);
						static::db_insert($user_id, "Bio_Course", $term_id, 0, $course->body->$cname, 1);
						return "/";
					}
					

					// тип товара
					// code of goods
					$shp_item = $args["course_id"];

					// предлагаемая валюта платежа
					// default payment e-currency
					$in_curr = "";

					// язык
					// language
					$culture = "ru";

					// кодировка
					// encoding
					$encoding = "utf-8";

					// формирование подписи
					// generate signature
					$crc  = md5("$mrh_login:$out_summ:$inv_id:$mrh_pass1:Shp_item=$shp_item:Shp_user=$user_id");

					// HTML-страница с кассой
					// ROBOKASSA HTML-page
					return "https://auth.robokassa.ru/Merchant/Index.aspx?".
						"MerchantLogin=$mrh_login&OutSum=$out_summ&InvId=$inv_id&IncCurrLabel=$in_curr".
						"&Description=$inv_desc&SignatureValue=$crc&Shp_item=$shp_item&Shp_user=$user_id&IsTest=1".
						"&Culture=$culture&Encoding=$encoding";				
				}
			] 
		);

		PEGraphql::add_object_type([
			'name' => 'MPaymentNew',
			'fields' => apply_filters(
				"pe_graphql_mpaymentnew_fields", 
				[
					'id' => Type::string(),
					//'user_id' => PEGraphql::object_type("User"),
					'element_id' => Type::int(),
					'sum' => Type::float(),
					'date' => Type::string(),
					'element_type' => Type::string(),
					'is_success' => Type::boolean(),
					'title' => Type::string(),
				],
				false
			),
			
		]);
	}

	static function exec_result_robocassa()
	{
		if(preg_match('#^/robokassa#', $_SERVER["REQUEST_URI"])) 
		{		
			// регистрационная информация (пароль #2)
			// registration info (password #2)
			$mrh_pass2 = Bio::$options['robocassa_password_2'];

			//установка текущего времени
			//current date
			$tm=getdate(time()+9*3600);
			$date="$tm[year]-$tm[mon]-$tm[mday] $tm[hours]:$tm[minutes]:$tm[seconds]";

			// чтение параметров
			// read parameters
			$out_summ = $_REQUEST["OutSum"];
			$inv_id = $_REQUEST["InvId"];
			$shp_item = $_REQUEST["Shp_item"];
			$shp_user = $_REQUEST["Shp_user"];
			$crc = $_REQUEST["SignatureValue"];

			$crc = strtoupper($crc);

			$my_crc = strtoupper(md5("$out_summ:$inv_id:$mrh_pass2:Shp_item=$shp_item:Shp_user=$shp_user"));
			
			$className 	= Bio::$options['goods_type'] ? Bio::$options['goods_type'] : "Bio_Course";
			$course 	= $className::get_instance( $shp_item );
			$cname 		= $className::get_title_name();
			$price	= $course->get_meta("price");
			
			// проверка корректности подписи
			// check signature
			
			if ($my_crc !=$crc)
			{
				do_action("bio_payment_bad_connection", $shp_item, $shp_user, $my_crc, $crc);
				echo "bad sign\n";
				file_put_contents(__DIR__ . "/log.txt", "$out_summ:$inv_id:$mrh_pass2:Shp_item=$shp_item:Shp_user=$shp_user\nbad sign\n" . print_r($_REQUEST, true), FILE_APPEND);
				exit();
			}
			
				
			
//			file_put_contents(__DIR__ . "/log.txt", print_r($_REQUEST, true), FILE_APPEND);
//			file_put_contents(__DIR__ . "/log.txt", print_r(get_term_meta($shp_item, "price", true), true), FILE_APPEND);
			if(!$course->is_enabled())
			{
			//	do_action("bio_payment_bad_goods", $shp_item, $shp_user);
				file_put_contents(__DIR__ . "/log.txt", "bad goods\n" . print_r($_REQUEST, true), FILE_APPEND);
				echo "bad goods\n";
				exit();
			}
			
		
			if(floatval($price) != floatval($out_summ))
			{
				//do_action("bio_payment_bad_goods_price", $shp_item, $shp_user, $out_summ);
				file_put_contents(__DIR__ . "/log.txt", "bad price\n" . print_r($_REQUEST, true), FILE_APPEND);
				echo "bad price\n";
				exit();
			}
			do_action("bio_payment_success", $shp_item, $shp_user, $out_summ );
			
			// признак успешно проведенной операции
			// success
			echo "OK$inv_id\n";
		}
	}

	
	
	static function exec_graphql_yandex_money()
	{
		PEGraphql::add_query( 
			'getRobokassaRedirectUrl', 
			[
				'description' => __( '', BIO ),
				'type' 		=> Type::string(),
				'args'     	=> [ 
					"course_id" => [ "type" => Type::int() ],
				],
				'resolve' 	=> function( $root, $args, $context, $info ) use($class_name, $post_type, $val)
				{		
					$mrh_login = Bio::$options['robocassa_key'];
					$mrh_pass1 = Bio::$options['robocassa_password_1'];

					// номер заказа
					// number of order
					$inv_id = 0;

					// описание заказа
					// order description
					$term_id 	= $args['course_id'];
					$className 	= Bio::$options['goods_type'] ? Bio::$options['goods_type'] : "Bio_Course";
					$course 	= $className::get_instance($args['course_id']);
					$cname 		= $className::get_title_name();
					$inv_desc 	= $course->body->$cname;//"ROBOKASSA Advanced User Guide";
					
					// сумма заказа
					// sum of order
					$out_summ = $course->get_meta("price");//"8.96";
					$user_id = get_current_user_id();
					
					//wp_die( $out_summ );
					
					if( !$out_summ )
					{
						//$prev_course_id 	= get_user_meta($user_id, "current_course", true );
						//pe_payment::set_user_current_course( $user_id, $term_id, $prev_course_id);
						static::db_insert($user_id, "Bio_Course", $term_id, 0, $course->body->$cname, 1);
						return "/";
					}
					

					// тип товара
					// code of goods
					$shp_item = $args["course_id"];

					// предлагаемая валюта платежа
					// default payment e-currency
					$in_curr = "";

					// язык
					// language
					$culture = "ru";

					// кодировка
					// encoding
					$encoding = "utf-8";

					// формирование подписи
					// generate signature
					$crc  = md5("$mrh_login:$out_summ:$inv_id:$mrh_pass1:Shp_item=$shp_item:Shp_user=$user_id");

					// HTML-страница с кассой
					// ROBOKASSA HTML-page
					return "https://auth.robokassa.ru/Merchant/Index.aspx?".
						"MerchantLogin=$mrh_login&OutSum=$out_summ&InvId=$inv_id&IncCurrLabel=$in_curr".
						"&Description=$inv_desc&SignatureValue=$crc&Shp_item=$shp_item&Shp_user=$user_id&IsTest=1".
						"&Culture=$culture&Encoding=$encoding";				
				}
			] 
		);

		PEGraphql::add_object_type([
			'name' => 'MPaymentNew',
			'fields' => apply_filters(
				"pe_graphql_mpaymentnew_fields", 
				[
					'id' => Type::string(),
					//'user_id' => PEGraphql::object_type("User"),
					'element_id' => Type::int(),
					'sum' => Type::float(),
					'date' => Type::string(),
					'element_type' => Type::string(),
					'is_success' => Type::boolean(),
					'title' => Type::string(),
				],
				false
			),
			
		]);
	}

	static function exec_result_yandex_money()
	{
		if(preg_match('#^/yandex#', $_SERVER["REQUEST_URI"])) 
		{
			header("HTTP/1.1 200 OK");
			status_header(200);
			ini_set("display_errors", 0);
			error_reporting(0);
			file_put_contents(__DIR__ . "/log.txt", print_r($_REQUEST, true), FILE_APPEND);		
			// регистрационная информация (пароль #2)
			// registration info (password #2)
			$mrh_pass2 = Bio::$options['robocassa_password_2'];

			//установка текущего времени
			//current date
			$tm=getdate(time()+9*3600);
			$date="$tm[year]-$tm[mon]-$tm[mday] $tm[hours]:$tm[minutes]:$tm[seconds]";

			// чтение параметров
			// read parameters
			$out_summ = $_REQUEST["orderSumAmount"];
			//$inv_id = $_REQUEST["InvId"];
			$shp_item = $_REQUEST["Shp_item"];
			$shp_user = $_REQUEST["Shp_user"];
			$crc = $_REQUEST["md5"];

			$crc = strtoupper($crc);

			$settings = json_decode(file_get_contents(__DIR__ . "/settings.json"), true);

			$my_crc = strtoupper(md5("{$_REQUEST["action"]};{$_REQUEST["orderSumAmount"]};{$_REQUEST["orderSumCurrencyPaycash"]};{$_REQUEST["orderSumBankPaycash"]};{$_REQUEST["shopId"]};{$_REQUEST["invoiceId"]};{$_REQUEST["customerNumber"]};{$settings["shopPassword"]}"));
			
			$className 	= Bio::$options['goods_type'] ? Bio::$options['goods_type'] : "Bio_Course";
			$course 	= $className::get_instance( $shp_item );
			$cname 		= $className::get_title_name();
			$price	= $course->get_meta("price");
			
			// проверка корректности подписи
			// check signature
			
			if ($my_crc !=$crc)
			{
				do_action("bio_payment_bad_connection", $shp_item, $shp_user, $my_crc, $crc);
				echo "bad sign\n";
				file_put_contents(__DIR__ . "/log.txt", "$crc$my_crc\nbad sign\n" . print_r($_REQUEST, true), FILE_APPEND);
				exit();
			}
			
				
			
//			file_put_contents(__DIR__ . "/log.txt", print_r($_REQUEST, true), FILE_APPEND);
//			file_put_contents(__DIR__ . "/log.txt", print_r(get_term_meta($shp_item, "price", true), true), FILE_APPEND);
			if(!$course->is_enabled())
			{
			//	do_action("bio_payment_bad_goods", $shp_item, $shp_user);
				file_put_contents(__DIR__ . "/log.txt", "bad goods\n" . print_r($_REQUEST, true), FILE_APPEND);
				echo "bad goods\n";
				exit();
			}
			
		
			if(floatval($price) != floatval($out_summ))
			{
				//do_action("bio_payment_bad_goods_price", $shp_item, $shp_user, $out_summ);
				file_put_contents(__DIR__ . "/log.txt", "bad price\n" . print_r($_REQUEST, true), FILE_APPEND);
				echo "bad price\n";
				exit();
			}
			do_action("bio_payment_success", $shp_item, $shp_user, $out_summ );
			
			// признак успешно проведенной операции
			// success
			file_put_contents(__DIR__ . "/log.txt", '<?xml version="1.0" encoding="UTF-8"?><' . $_REQUEST["action"] . 'Response performedDatetime="' . date("c") .
			'" code="0" invoiceId="' . $_REQUEST["invoiceId"] . '" shopId="' . $settings["shopId"] . '"/>', FILE_APPEND);
			echo '<?xml version="1.0" encoding="UTF-8"?><' . $_REQUEST["action"] . 'Response performedDatetime="' . date("c") .
				'" code="0" invoiceId="' . $_REQUEST["invoiceId"] . '" shopId="' . $settings["shopId"] . '"/>';
			die();
		}
	}

	static function db_insert($user_id, $element_type, $element_id, $sum, $title, $is_success)
	{
		global $wpdb;

		$user_id = intval($user_id);
		$element_id = intval($element_id);
		$is_success = intval($is_success);
		$sum = floatval($sum);
		$time = time();
		($data = [
			"user_id" => $user_id,
			"element_type" => $element_type,
			"element_id" => $element_id,
			"sum" => $sum,
			"date" => $time,
			"is_success" => $is_success,
			"title" => $title,
		]);
		$wpdb->insert("{$wpdb->prefix}payment", $data);
		
	}

	static function db_get_all($user_id = null)
	{
		global $wpdb;

		$user_id = intval($user_id);

		if ($user_id)
		{
			return $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "payment WHERE user_id = {$user_id} ORDER BY id DESC", ARRAY_A);
		}
		else
		{
			return $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . "payment ORDER BY id DESC", ARRAY_A);
		}
	}
	static function add_views_column( $columns )
	{
		$columns = parent::add_views_column( $columns );
		unset($columns['cb']);
		unset($columns['title']);
		unset($columns['element_id']);
		unset($columns['element_type']);
		$columns = array_merge(
			[
				"cb" 				=> " ",
				"title" 			=> __("Title"),
				'element' 			=> __("Goods", MCOURSES)
			], 
			$columns
		);
		return $columns;
	}
	static function fill_views_column($column_name, $post_id) 
	{
		require_once(_REAL_PATH."class/SMC_Object_type.php");				
		$SMC_Object_type	= SMC_Object_type::get_instance();
		
		$p				= static::get_instance($post_id);
		switch($column_name)
		{
			case "element":
				$class 	= $p->get_meta("element_type");
				
				$matrix_type 	= $SMC_Object_type->get_matrix_by_class($class);
				$matrix 		= $SMC_Object_type->object[ $matrix_type ];
				if($matrix['t']['type'] == "post")
				{
					$post_type = get_post_type_object(  $matrix_type );					
					$pt 	= $post_type->labels;
					$elem 	= $class::get_instance( $p->get_meta("element_id") );
					$title 	= $elem->is_enabled() ? $elem->body->post_title : "this element not enabled.";
				}
				else
				{
					$tax 	= get_taxonomy( $matrix_type );
					$pt 	= get_taxonomy_labels( $tax );
					$elem 	= $class::get_instance( $p->get_meta("element_id") );
					$title 	= $elem->is_enabled() ? $elem->body->name : "this element not enabled.";
				}
				//print_r( $tax );
				echo "<div class='bsingular_name'>". $pt->singular_name . " </div><div class='btitle'>" . $title . "</div>";
				break;
			default:
				parent::fill_views_column($column_name, $post_id);
		}
	}
	static function register_all()
	{
		$labels = array(
			'name'               => __("Payment", MCOURSES), // Основное название типа записи
			'singular_name'      => __("Payment", MCOURSES), // отдельное название записи типа Book
			'add_new'            => __("add Payment", MCOURSES), 
			'all_items' 		 => __('Payments', MCOURSES),
			'add_new_item'       => __("add Payment", MCOURSES), 
			'edit_item'          => __("edit Payment", MCOURSES), 
			'new_item'           => __("add Payment", MCOURSES), 
			'view_item'          => __("see Payment", MCOURSES), 
			'search_items'       => __("search Payment", MCOURSES), 
			'not_found'          => __("no Payments", MCOURSES), 
			'not_found_in_trash' => __("no Payments in trash", MCOURSES), 
			'parent_item_colon'  => '',
			'menu_name'          => __("Payments", MCOURSES), 
		);
		register_post_type(
			static::get_type(), 
			[
				'labels'             => $labels,
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => 'pe_payment_page',
				'query_var'          => true,
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				'menu_position'      => 1,
				"menu_icon"			 => "dashicons-megaphone",
				'supports'           => array( 'title', "editor", "author" ),
				"rewrite"			 => ["slug" => ""]
			]
		);
	}
}